<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/14/19
 * Time: 11:37 PM
 */

namespace App\LaravelMenuManager\Delete;

interface IDeleteEnum
{

    public function getDeleteEnum() : Int;

}
