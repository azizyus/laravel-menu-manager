
<input type="hidden" name="itemGroup" value="{{$menu->itemGroup}}">

<div class="col-md-12">

    <ul class="nav nav-tabs">
        @foreach($languages as $language)

            <li class="nav-item @if($loop->first) active @endif" >

                <a class="nav-link @if($loop->first) active @endif" data-toggle="tab" href="#{{$language->shortTitle}}">{{$language->title}}</a>


            </li>


        @endforeach
    </ul>

    <div class="col-md-12">

        <div class="tab-content px-1 pt-1">
            @foreach($languages as $language)
                <div id="{{$language->shortTitle}}" class="tab-pane  @if($loop->first) active @endif">




                    <div class="form-group">
                        <label for="title-{{$language->id}}">Title</label>
                        <input value="{{$menu->getTranslated($language)->title}}" type="text" name="title-{{$language->id}}" id="title-{{$language->id}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="link-{{$language->id}}">Link</label>
                        <input value="{{$menu->getTranslated($language)->link}}" type="text" name="link-{{$language->id}}" id="link-{{$language->id}}" class="form-control">
                    </div>




                </div>
            @endforeach

        </div>


    </div>
    <div class="col-md-12">
        <input type="submit" class="btn btn-md btn-primary pull-right" name="addCustomMenuItem" value="@if($menu->exists) Update @else Add @endif as Custom Menu Item">
    </div>

</div>




<div class="col-md-12 mt-5">
 <div class="col-md-12">
     <div class="form-group">
         <label for="pages">Item</label>
         <select class="form-control" name="menuItem" id="menuItem">

             @foreach(config("menus.menus") as $m)


                 <optgroup label="{{$m["title"]}}">
                        @foreach($menuManager->getMenuSupplierVomitByEnum($m["enum"]) as $menuItem)
                         <option @if($menu->type == $m["enum"] && $menu->itemId == $menuItem->id()) selected="selected" @endif value="{{$menuItem->id()}}-{{$m["enum"]}}">{{$menuItem->title()}}</option>
                        @endforeach
                 </optgroup>

             @endforeach

             {{--<optgroup label="Pages">--}}
             {{--@foreach($pages as $page)--}}
             {{--<option @if($page->id==$menu->itemId && MenuManager::_TYPE_PAGE==$menu->type ) selected="selected" @endif value="{{$page->id}}-{{MenuManager::_TYPE_PAGE}}">{{$page->getLanguageProperty("title")}}</option>--}}
             {{--@endforeach--}}
             {{--</optgroup>--}}

             {{--<optgroup label="Tours">--}}
             {{--@foreach($tours as $tour)--}}
             {{--<option @if($tour->id==$menu->itemId && MenuManager::_TYPE_TOUR==$menu->type ) selected="selected" @endif  value="{{$tour->id}}-{{MenuManager::_TYPE_TOUR}}">{{$tour->getLanguageProperty("title")}}</option>--}}
             {{--@endforeach--}}
             {{--</optgroup>--}}


             {{--<optgroup label="Categories">--}}
             {{--@foreach($categories as $category)--}}
             {{--<option @if($category->id==$menu->itemId && MenuManager::_TYPE_CATEGORY==$menu->type ) selected="selected" @endif value="{{$category->id}}-{{MenuManager::_TYPE_CATEGORY}}">{{$category->getLanguageProperty("title")}}</option>--}}
             {{--@endforeach--}}
             {{--</optgroup>--}}

         </select>
     </div>
     <input type="submit" class="btn btn-md btn-primary pull-right" name="addMenuItem" value="@if($menu->exists) Update @else Add @endif as pre routed item">

 </div>

</div>
