@extends($layoutBladeFilePath)


@section($contentSection)


    <div class="card">

        <div class="card-content">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12 mb-3">
                        <a class="btn btn-primary btn-md" href="{{route("menuBuilder.sortEdit",['itemGroup'=>$menu!=null ? $menu->itemGroup : null])}}">Go to your menu</a>

                    </div>
                    <div class="col-md-12">


                        @if($menu->id==null)
                            <form action="{{route("menuBuilder.store")}}" method="POST">
                                @else
                                    <form action="{{route("menuBuilder.update")}}" method="POST">
                                        @endif
                                        {{csrf_field()}}
                                        <input type="hidden" name="id" value="{{$menu->id}}">


                                        @include("LMS::menu_crud")




                                    </form>


                    </div>
                </div>
            </div>

        </div>

    </div>

@endsection
