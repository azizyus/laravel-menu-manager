<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/5/19
 * Time: 2:58 AM
 */

namespace App\LaravelMenuManager\Suppliers;




interface ISupplier
{


    /**
     * @return array
     *
     * returns collection your items implemented by ISupplierItem
     * something like; return $collection->mapInto(ISupplierItem:class);
     * and probably ISupplierItem takes your EloquentModel as constructor param so read doc about mapInto
     */
    public function vomit() : array;

}