<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/5/19
 * Time: 3:18 AM
 */

namespace App\LaravelMenuManager\Suppliers\SupplierResults;


interface ISupplierItem
{

    public function title(); //because of multi lang this one could be null
    public function id() : Int;

}