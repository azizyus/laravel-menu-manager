<?php



#MENU#
Route::get("menu-builder-edit-sort",["as"=>"menuBuilder.sortEdit","uses"=>"MenuBuilderController@sortEdit"]);
Route::get("menu-builder-save",["as"=>"menuBuilder.save","uses"=>"MenuBuilderController@save"]);
Route::get("menu-builder-add",["as"=>"menuBuilder.add","uses"=>"MenuBuilderController@add"]);
Route::post("menu-builder-store",["as"=>"menuBuilder.store","uses"=>"MenuBuilderController@store"]);
Route::get("menu-builder-edit",["as"=>"menuBuilder.edit","uses"=>"MenuBuilderController@edit"]);
Route::post("menu-builder-update",["as"=>"menuBuilder.update","uses"=>"MenuBuilderController@update"]);
Route::get("menu-builder-delete",["as"=>"menuBuilder.delete","uses"=>"MenuBuilderController@delete"]);
#MENU#
