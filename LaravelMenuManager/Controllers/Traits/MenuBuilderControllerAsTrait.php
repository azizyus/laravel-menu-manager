<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 8.01.2019
 * Time: 15:35
 */

namespace App\LaravelMenuManager\Controllers\Traits;


use App\Http\Controllers\Admin\AdminController;
use App\LaravelMenuManager\Factory\MenuFactory;
use App\LaravelMenuManager\MenuManager;
use App\LaravelMenuManager\Models\LaravelMenuManagerItem;
use App\LaravelMenuManager\Models\LaravelMenuManagerItemExtended;
use App\LaravelMenuManager\Models\LaravelMenuManagerItemExtendedDefault;
use Azizyus\LaravelLanguageHelper\App\Models\LanguageWithoutDeleted;
use Azizyus\LaravelLanguageHelper\App\Repositories\Eloquent\LanguageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

trait MenuBuilderControllerAsTrait
{

    public $menuManager=null;
    public $menuManagerLanguageRepository;
    protected $menuFactory;
    protected $group = null;
    public function __constructTrait(MenuManager $menuManager,LanguageRepository $languageRepository, MenuFactory $menuFactory)
    {

        $this->menuManager = $menuManager;
        $this->menuManagerLanguageRepository = $languageRepository;
        $this->menuFactory = $menuFactory;

        $this->group = \request()->get('itemGroup',null);
        $this->menuFactory->setGroup($this->group);
        $this->menuManager->setGroup($this->group);
        View::share([
            "menuManager" => $this->menuManager,
            "scriptSection" => config("menus.scriptSection"),
            "styleSection" => config("menus.styleSection"),
            "contentSection" => config("menus.contentSection"),
            "layoutBladeFilePath" => config("menus.layoutBladeFilePath"),
            'itemGroup' => $this->group
        ]);
    }

    public function add(Request $request)
    {



        $data=[
            "menu" => $this->menuFactory->makeInstance(),
            "languages" => $this->menuManagerLanguageRepository->getAll(),
        ];


        return view("LMS::add")->with($data);



    }


    public function delete(Request $request)
    {
        $id = $request->get("id");
        $deletedMenu = $this->menuManager->menuRepository->delete($id);
        return Redirect::to(route("menuBuilder.sortEdit",['itemGroup'=>$request->get('itemGroup',$deletedMenu->itemGroup)]));
    }

    public function update(Request $request)
    {



        $id = $request->get("id");
        $this->menuManager->menuRepository
            ->updateOrInsert($request,$id);



        return Redirect::to(route("menuBuilder.sortEdit",['itemGroup'=>$request->get('itemGroup',null)]));



    }



    public function store(Request $request)
    {



        $this->menuManager->menuRepository->updateOrInsert($request);

        return Redirect::to(route("menuBuilder.sortEdit",['itemGroup'=>$request->get('itemGroup',null)]));



    }



    public function edit(Request $request)
    {
        $id = $request->get("id");
        $menu = $this->menuManager->menuRepository->getMenu($id);


        $data=[
            "menu"=>$menu,
            "languages" => $this->menuManagerLanguageRepository->getAll(),

        ];


        return view("LMS::add")->with($data);

    }

    public function sortEdit(Request $request)
    {




        $tree = $this->menuManager->getTreeForPanel();


        $data=[

            "tree"=>$tree,

        ];

        return view("LMS::order_edit")->with($data);

    }



    public function save(Request $request)
    {
        $this->menuManager->saveForPanel($request);
    }

}
