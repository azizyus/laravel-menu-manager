<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/23/18
 * Time: 4:37 PM
 */

namespace App\LaravelMenuManager;


class ConfigHelperResult
{


    public $implementationNamespace;
    public $enum;
    public $title;
    public $supplier;
    /**
     * ConfigHelperResult constructor.
     *
     * @param $implementationNamespace
     * @param $enum
     */
    public function __construct($menu)
    {
        $this->implementationNamespace = $menu["imp"];
        $this->enum = $menu["enum"];
        $this->title = $menu["title"];
        $this->supplier = $menu["supplier"];
    }


}