<?php

namespace App\LaravelMenuManager;

use App\LaravelMenuManager\Cache\CacheKey;
use App\LaravelMenuManager\Cache\IMenuCacheable;
use App\LaravelMenuManager\Delete\IDeleteEnum;
use App\LaravelMenuManager\Repository\MenuRepository;
use App\LaravelMenuManager\Repository\MenuRepositoryDefault;
use App\LaravelMenuManager\Singleton\MenuConfigHelperSingleton;
use Azizyus\LaravelLanguageHelper\App\Models\Translation;
use Azizyus\LaravelLanguageHelper\App\Repositories\Eloquent\LanguageRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class LaravelMenuManagerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {


        $this->loadMigrationsFrom(__DIR__."/Migrations");

        $this->loadViewsFrom(__DIR__."/Html","LMS");

        $this->publishes([


            __DIR__."/Config/menus.php" => config_path("menus.php")

        ],"azizyus/laravel-menu-manager-publish-config");

        $this->publishes([


            __DIR__."/Assets" => public_path("menu")

        ],"azizyus/laravel-menu-manager-publish-assets");


        $this->publishes([


            __DIR__."/Html" => resource_path("menu")

        ],"azizyus/laravel-menu-manager-publish-html");




        try
        {
            DB::connection()->getPdo();
        $menuRepository = new MenuRepositoryDefault();
        $configManager = MenuConfigHelperSingleton::get();
        $languages = ( new LanguageRepository())->getAll();

        Event::listen(['eloquent.saved: *'], function($nameSpace,$models) use ($menuRepository,$configManager,$languages) {

            $cacheGroups = config('menus.cacheGroups');
            foreach ($models as $model)
            {

                //while updating menu itself or something can place in menu, cache should be cleared
                if($model instanceof IDeleteEnum || $model instanceof  IMenuCacheable)
                {

                    foreach ($languages as $language)
                    {
                        foreach ($cacheGroups as $cg) Cache::forget(CacheKey::make($language,$cg));
                        Cache::forget(CacheKey::make($language));
                    }

                }

                if($model instanceof Translation)
                {

                    $enum = $model->tableEnum;
                    $menuOptions = $configManager->getByEnum($enum);


                    //that means we re adding or updating something can be placed in menu
                    //they should be use same enum with translates otherwise it wont work
                    if($menuOptions != null)
                    {
                        foreach ($languages as $language)
                        {
                            foreach ($cacheGroups as $cg) Cache::forget(CacheKey::make($language,$cg));
                            Cache::forget(CacheKey::make($language));
                        }

                    }






                }

            }


        });

        Event::listen(['eloquent.deleting: *'],function ($nameSpace,$models) use($menuRepository,$languages) {

            $cacheGroups = config('menus.cacheGroups');
            foreach ($models as $model)
            {


                //while deleting something can place in menus
                //cache should be cleared
                if($model instanceof IDeleteEnum)
                {

                    $menuRepository->deleteViaEnum($model->getDeleteEnum(),$model->id);

                    foreach ($languages as $language)
                    {
                        foreach ($cacheGroups as $cg) Cache::forget(CacheKey::make($language,$cg));
                        Cache::forget(CacheKey::make($language));
                    }


                }

                //while deleting menu itself, cache should be cleared
                if($model instanceof  IMenuCacheable)
                {

                    foreach ($languages as $language)
                    {
                        foreach ($cacheGroups as $cg) Cache::forget(CacheKey::make($language,$cg));
                        Cache::forget(CacheKey::make($language));
                    }

                }

            }

        });
        }
        catch (\Exception $exception)
        {
                 Log::info("There is no db connection so could not run menu manager cache events");
        }
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }
}
