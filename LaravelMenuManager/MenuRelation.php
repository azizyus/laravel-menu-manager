<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/23/18
 * Time: 4:45 PM
 */

namespace App\LaravelMenuManager;



use App\LaravelMenuManager\Singleton\MenuConfigHelperSingleton;

trait MenuRelation
{
    public $relationColumn = "itemId";

    public function getMenuEnum()
    {
        return $this->type;
    }

    public function relation()
    {
        $configHelper = MenuConfigHelperSingleton::get();
        $configHelperResult = $configHelper->getByEnum($this->getMenuEnum());
        return $this->hasOne($configHelperResult->implementationNamespace,"id",$this->relationColumn);
    }

}