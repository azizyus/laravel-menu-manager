<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 24.08.2018
 * Time: 14:37
 */

namespace App\LaravelMenuManager\Repository;


use App\LaravelMenuManager\Cache\CacheKey;
use App\LaravelMenuManager\Cache\IMenuCacheable;
use App\LaravelMenuManager\Factory\MenuFactory;
use App\LaravelMenuManager\MenuEnums;
use App\LaravelMenuManager\Models\LaravelMenuManagerItemExtended;

use Azizyus\LaravelLanguageHelper\App\Models\ILanguage;
use Azizyus\LaravelLanguageHelper\App\Models\Language;
use Azizyus\LaravelLanguageHelper\App\Repositories\Eloquent\LanguageRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;


abstract class MenuRepository
{

    protected $group;
    public $languageRepository;
    public function __construct()
    {

        $this->languageRepository = new LanguageRepository();

    }

    public function delete($id)
    {
        $menu = $this->getMenu($id);
        //otherwise sub items probably will be ghost
        $this->baseQuery()->where("parent",$menu->id)->update(["parent"=>0]);
        $menu->delete();
        return $menu;
    }

    public function all()
    {
        return $this->baseQuery()->get();
    }

    abstract public function baseModel() : LaravelMenuManagerItemExtended;

    public function baseQuery()
    {
        $query = $this->baseModel()->with("translate")->orderBy("SORT","ASC");
        $query->withGlobalScope('itemGroup',new GroupScope($this->group));
        return $query;
    }

    public function getMenu($id)
    {


        $menu = $this->baseQuery()->withoutGlobalScope('itemGroup')->where("id",$id)->first();

        return $menu;


    }

    public function getById($id)
    {
        return $this->getMenu($id);
    }


    public function recursive($all,$item)
    {
        $childs = $this->cacheableMethod($item);
        $childs["childs"] = [];
        foreach ($all->where("parent",$item->id)->all() as $itemSub)
        {
            $childs["childs"][] = $this->recursive($all,$itemSub);
        }



        return $childs;
    }


    public function allCacheCallback()
     {



        return function(){
            $all = $this->all();

            $array = [];

            foreach ($all as $item)
            {

                if($item->parent == 0)
                {

                    $childs = $this->recursive($all,$item);

                    $array[$item->id]=$childs;
                }

            }
           return $array;
        };



    }


    public function setGroup($group)
    {
        $this->group = $group;
    }

    public function allCacheOutput(ILanguage $language)
    {
        return Cache::remember(CacheKey::make($language,$this->group),config("menus.cacheTimeout"),$this->allCacheCallback());
    }



    private function cacheableMethod(IMenuCacheable $cacheable)
    {

        return $cacheable->cacheOutput();

    }


    public function deleteViaEnum($enum,$id)
    {


        $menu = $this->baseQuery()->where("type",$enum)->where("ItemId",$id)->first();

        if($menu)
        {

            $menu->delete();

        }


    }



    public function updateOrInsert(Request $request,$id=null)
    {



        $menu = $this->getById($id);

        if($menu==null) $menu = (new MenuFactory())->makeInstance(); //i dont really need set to groups because it would loaded from request


        $menu->itemGroup = $request->get('itemGroup',null); //its nullable column and null is default

        $languages = $this->languageRepository->getAll();


        //if you select from list
        if ($request->get("addMenuItem") != "")
        {
            $menuItem = $request->get("menuItem");

            list($itemId, $itemType) = explode("-", $menuItem);


            $menu->type = $itemType;
            $menu->itemId = $itemId;
            $menu->save();
        }
        //if you fill damn inputs and add custom link
        elseif($request->get("addCustomMenuItem") != "")
        {

            $menu->type = MenuEnums::_TYPE_DROP_DOWN;
            $menu->itemId = 0;
            $menu->save();
            foreach ($languages as $language)
            {


                $languageId = $language->id;
                $title = $request->get("title-$languageId");
                $link = $request->get("link-$languageId");

                $menu->updateTranslation($language,[

                    "title" => $title,
                    "link" => $link,

                ]);



            }



        }


    }

}
