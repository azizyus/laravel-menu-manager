<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/14/19
 * Time: 11:41 PM
 */

namespace App\LaravelMenuManager\Repository;


use App\LaravelMenuManager\Models\LaravelMenuManagerItemExtended;
use App\LaravelMenuManager\Models\LaravelMenuManagerItemExtendedDefault;

class MenuRepositoryDefault extends MenuRepository
{

    public function baseModel(): LaravelMenuManagerItemExtended
    {
        return new LaravelMenuManagerItemExtendedDefault();
    }

}
