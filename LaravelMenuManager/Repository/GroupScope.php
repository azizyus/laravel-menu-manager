<?php


namespace App\LaravelMenuManager\Repository;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class GroupScope implements Scope
{

    protected $group;

    public function __construct($group = null)
    {
        $this->group = $group;
    }

    public function apply(Builder $builder, Model $model)
    {
        $builder->where('itemGroup',$this->group);
    }

}
