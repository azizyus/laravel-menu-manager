<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 17.01.2019
 * Time: 13:35
 */

namespace App\LaravelMenuManager\Cache;


use Azizyus\LaravelLanguageHelper\App\Models\ILanguage;
use Azizyus\LaravelLanguageHelper\App\Models\Language;

class CacheKey
{

    public static function make(ILanguage $language,$group=null)
    {
        $groupCacheKey = "null";
        if($group != null)
            $groupCacheKey=$group;
        return "menu_manager_cache_{$language->getId()}_$groupCacheKey".md5(request()->getHost());
    }

}
