<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 8.01.2019
 * Time: 17:37
 */

namespace App\LaravelMenuManager\Cache;



interface IMenuCacheable
{

    public function cacheOutput();

}