<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaravelMenuManagerItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laravel_menu_manager_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("parent");
            $table->unsignedInteger("sort");
            $table->unsignedInteger("type")->nullable();
            $table->unsignedInteger("itemId");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('laravel_menu_manager_items');
    }
}
