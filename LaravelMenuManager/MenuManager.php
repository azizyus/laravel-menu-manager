<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 24.08.2018
 * Time: 10:36
 */

namespace App\LaravelMenuManager;


use App\LaravelMenuManager\Repository\MenuRepository;


use App\LaravelMenuManager\Singleton\MenuConfigHelperSingleton;
use Azizyus\LaravelLanguageHelper\App\Models\LanguageWithoutDeleted;
use Illuminate\Http\Request;


class MenuManager
{

    public $rowParser;
    public $menuRepository;
    public $defaultPaneLanguage;
    public function __construct(MenuRepository $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    public function setGroup($group)
    {
        $this->menuRepository->setGroup($group);
    }

    public function setDefaultPanelLanguage(LanguageWithoutDeleted $language)
    {
       $this->defaultPaneLanguage = $language;
    }

    public function getMenuSupplierVomitByEnum($enum)
    {
        $configManager = MenuConfigHelperSingleton::get();
        $supplier = $configManager->getSupplierInstance($enum);
        if($supplier)
        {
            return $supplier->vomit();
        }
        throw new \Exception("I CANT FIND YOUR MENU SUPPLIER");
    }

    function get_menu($items,$class = 'dd-list') {



        $html = "<ol class=\"".$class."\" id=\"menu-id\">";

        foreach($items as $key=>$value) {
            $html.= '<li class="dd-item dd3-item" data-id="'.$value['id'].'" >
                                         <div class="dd-handle dd3-handle"></div>
                                         <div class="dd3-content">
                                         <span id="label_show'.$value['id'].'"><a href="edit">'.$value['label'].'</a></span>
                                                <a href="'.route("menuBuilder.delete",["id"=>$value["id"]]).'" onclick="confirm(`Are you sure  delete '.$value['label'].'?`);" class=" pull-right" >Delete</a>
                                                <a  href="'.route("menuBuilder.edit",["id"=>$value["id"]]).'" class="pull-right mr-1">Edit</a>
                                          </div>';



            if(array_key_exists('child',$value)) {
                $html .= $this->get_menu($value['child'],'child');
            }
            $html .= "</li>";
        }
        $html .= "</ol>";

        return $html;

    }

    function parseJsonArray($jsonArray, $parentID = 0) {

        $return = array();
        foreach ($jsonArray as $subArray) {
            $returnSubSubArray = array();
            if (isset($subArray->children)) {
                $returnSubSubArray = $this->parseJsonArray($subArray->children, $subArray->id);
            }

            $return[] = array('id' => $subArray->id, 'parentID' => $parentID);
            $return = array_merge($return, $returnSubSubArray);
        }
        return $return;
    }




    public function getTreeForPanel()
    {
        $items=[];
        $idParam="id";
        $menuElements = $this->menuRepository->all();


        foreach($menuElements as $data) {



            $thisRef = &$ref[$data->$idParam];


            $thisRef['parent'] = $data->parent;
            $thisRef['label'] = $data->getPanelTitle($this->defaultPaneLanguage);
            $thisRef['link'] = "";
            $thisRef['id'] = $data->$idParam;
            $thisRef["type"] = $data->type;
            $thisRef["itemId"] = $data->itemId;






            if($data->parent == 0) {
                $items[$data->$idParam] = &$thisRef;
            } else {
                $ref[$data->parent]['child'][$data->$idParam] = &$thisRef;
            }

        }


        $tree = $this->get_menu($items);

        return $tree;


    }

    public function saveForPanel(Request $request)
    {

        $data = json_decode($request->data);


        $readbleArray = $this->parseJsonArray($data);

        $i=0;
        foreach($readbleArray as $row){
            $i++;


            $menuItem = $this->menuRepository->getById($row["id"]);

            if($menuItem)
            {
                $menuItem->parent = $row["parentID"];
                $menuItem->sort = $i;

                $menuItem->save();

            }
//            $db->exec("update kategoriler set parent = '".$row['parentID']."', sort = '".$i."' where kategori_id = '".$row['id']."' ");
        }

    }

}
