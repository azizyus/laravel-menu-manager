<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/23/18
 * Time: 4:36 PM
 */

namespace App\LaravelMenuManager;


use App\LaravelMenuManager\Suppliers\ISupplier;
use Illuminate\Support\Facades\Log;

class MenuConfigHelper
{


    public $config;

    public function __construct()
    {
        $this->config = config("menus");
    }

    public function getInstanceByEnum($enum,$relatedInstance)
    {
        $configHelperResult = $this->getByEnum($enum);


        $implementationNameSpace = $configHelperResult->implementationNamespace;

        $implementationInstance = new $implementationNameSpace();

        return $implementationInstance;





    }

    public function getSupplierInstance($enum) : ISupplier
    {

        $configHelperResult = $this->getByEnum($enum);
        $namespace = $configHelperResult->supplier;
        return new $namespace();

    }

    public function getByEnum($enum) : ?ConfigHelperResult
    {


        foreach ($this->config["menus"] as $menu)
        {
            if($menu["enum"] == $enum)
            {
                return new ConfigHelperResult($menu);
            }

        }

        return null;

    }


}