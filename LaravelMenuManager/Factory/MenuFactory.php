<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/23/18
 * Time: 5:33 PM
 */

namespace App\LaravelMenuManager\Factory;


use App\LaravelMenuManager\Models\LaravelMenuManagerItemExtended;
use App\LaravelMenuManager\Models\LaravelMenuManagerItemExtendedDefault;

class MenuFactory
{

    protected $group = null;

    public function setGroup($group = null)
    {
        $this->group = $group;
    }


    public function makeInstance()
    {
        $menu = new LaravelMenuManagerItemExtendedDefault();
        $menu->parent = 0;
        $menu->sort = 0;
        $menu->itemGroup = $this->group;
        return $menu;
    }

}
