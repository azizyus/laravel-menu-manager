<?php

namespace App\LaravelMenuManager\Models;

use App\LaravelMenuManager\Cache\IMenuCacheable;
use App\LaravelMenuManager\MenuEnums;
use App\LaravelMenuManager\MenuRelation;
use Azizyus\LaravelLanguageHelper\App\Models\LanguageWithoutDeleted;
use Azizyus\LaravelLanguageHelper\App\Models\Traits\HasLanguageProperties;
//use App\Models\Traits\HasLanguagePropertiesProjectTrait;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $parent
 * @property int $sort
 * @property int $type
 * @property int $itemId
 * @property string $created_at
 * @property string $updated_at
 */
abstract class LaravelMenuManagerItemExtended extends LaravelMenuManagerItem implements IMenuCacheable
{

    protected $table = "laravel_menu_manager_items";

    use HasLanguageProperties;
    use MenuRelation;



    public function getPanelTitle()
    {
        if($this->relations==null || $this->type == MenuEnums::_TYPE_DROP_DOWN)
        {
            return $this->getMenuPanelTitle();
        }
        else
        {
            if($this->relation == null) return "[DELETED]";
            else return $this->relation->getMenuPanelTitle();
        }
    }

    abstract public function getMenuPanelTitle();
    abstract public function getMenuFrontTitle();
    abstract public function getMenuFrontLink();

    public function getFrontTitle()
    {
        if($this->relations==null || $this->type == MenuEnums::_TYPE_DROP_DOWN)
        {
            return $this->getMenuFrontTitle();
        }
        else
        {
            if($this->relation == null) return "[DELETED]";
            else return $this->relation->getFrontTitle();
        }
    }

    public function getLink()
    {
        if($this->relations==null || $this->type == MenuEnums::_TYPE_DROP_DOWN)
        {
            return $this->getMenuFrontLink();
        }
        else
        {
            if($this->relation == null) return "#";
            else return $this->relation->getFrontRoute();

        }
    }

    public function cacheOutput()
    {
        return [

            "title" => $this->getFrontTitle(),
            "link" => $this->getLink(),

        ];
    }

    public function getLanguageProperties()
    {
        return [
            "title",
            "link"
        ];
    }

    public function translateEnum()
    {
        return 9999;
    }



}
