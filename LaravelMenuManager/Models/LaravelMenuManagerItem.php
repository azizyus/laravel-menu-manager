<?php

namespace App\LaravelMenuManager\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $parent
 * @property int $sort
 * @property int $type
 * @property int $itemId
 * @property string $created_at
 * @property string $updated_at
 * @property int $itemGroup
 */
class LaravelMenuManagerItem extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['parent', 'sort', 'type', 'itemId', 'created_at', 'updated_at','itemGroup'];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'Y-m-d H:i:s';

    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql';

}
