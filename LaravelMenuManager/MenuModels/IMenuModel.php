<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 8.01.2019
 * Time: 14:02
 */

namespace App\LaravelMenuManager\MenuModels;


interface IMenuModel
{

    public function getMenuPanelTitle();
    public function getFrontTitle();
    public function getFrontRoute();
}