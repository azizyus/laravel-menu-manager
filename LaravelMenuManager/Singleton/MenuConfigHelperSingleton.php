<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 12/23/18
 * Time: 5:16 PM
 */

namespace App\LaravelMenuManager\Singleton;


use App\LaravelMenuManager\MenuConfigHelper;

class MenuConfigHelperSingleton
{


    public static $instance=null;
    public static function get()  : MenuConfigHelper
    {


        if(self::$instance==null){


            self::$instance = new MenuConfigHelper();

        }

        return self::$instance;

    }

}